package com.example.springfirstlesson.controller;

import com.example.springfirstlesson.aspects.Count;
import com.example.springfirstlesson.aspects.Period;
import com.example.springfirstlesson.dto.UserDto;
import com.example.springfirstlesson.filter.UserCriteria;
import com.example.springfirstlesson.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/user/v1")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @Period
    @Count
    @PostMapping
    public void add(@RequestBody Map<String, Object> request) {
        userService.add(request);
    }

    @Period
    @Count
    @GetMapping
    public List<UserDto> findAll(UserCriteria userCriteria) {
        return userService.findUsers(userCriteria);
    }
}
