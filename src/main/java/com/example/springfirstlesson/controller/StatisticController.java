package com.example.springfirstlesson.controller;

import com.example.springfirstlesson.aspects.StatisticAspect;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("api/statistic/v1")
@RequiredArgsConstructor
public class StatisticController {

    private final StatisticAspect customAspects;

    @GetMapping("/methods-period-avg")
    public Long getAllMethodsPeriodAvg() {
        return customAspects.periodAvgData;
    }

    @GetMapping("/most-used-methods")
    public Map<String, Integer> getAllMostUsedMethods() {
        return customAspects.mostUsedMethods;
    }
}
