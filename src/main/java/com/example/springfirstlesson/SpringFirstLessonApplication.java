package com.example.springfirstlesson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class SpringFirstLessonApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringFirstLessonApplication.class, args);
    }

}
