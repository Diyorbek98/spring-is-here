package com.example.springfirstlesson.filter;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

//@Builder
@Data
public class UserCriteria {

    Long id;
    String firstName;
    String lastName;
    String username;
    @JsonFormat(pattern = "yyyy-MM-dd")
    Date birthDate;
    int page = 0;
    int size = 10;
}
