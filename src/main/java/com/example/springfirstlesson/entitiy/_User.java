package com.example.springfirstlesson.entitiy;

import com.example.springfirstlesson.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.Date;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@FieldDefaults(level = PRIVATE)
public class _User implements Serializable {

    Long id;
    String firstName;
    String lastName;
    String username;

    @JsonFormat(pattern = "yyyy-MM-dd")
    Date birthDate;


    public static UserDto convert(_User domain) {
        return new UserDto(domain.getId(), domain.getFirstName(), domain.getLastName(), domain.getUsername(), domain.getBirthDate());
    }

}
