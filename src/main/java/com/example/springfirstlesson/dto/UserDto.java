package com.example.springfirstlesson.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.util.Date;

import static lombok.AccessLevel.PRIVATE;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@FieldDefaults(level = PRIVATE)
public class UserDto  implements Serializable {
    Long id;
    String firstName;
    String lastName;
    String username;

    Date birthDate;
}
