package com.example.springfirstlesson.service;

import com.example.springfirstlesson.aspects.Count;
import com.example.springfirstlesson.aspects.Period;
import com.example.springfirstlesson.dto.UserDto;
import com.example.springfirstlesson.entitiy._User;
import com.example.springfirstlesson.exception.JsonParsingException;
import com.example.springfirstlesson.filter.UserCriteria;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.hasText;

@Service
public class UserService {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final List<_User> userList = new ArrayList<>();

    @Period
    @Count
    public void add(Map<String, Object> request) {

        _User user = new _User();
        try {
            objectMapper.updateValue(user, request);
        } catch (JsonMappingException e) {
            throw new JsonParsingException("Couldn't be parsed!");
        }
        user.setId((long) userList.size() + 1);
        userList.add(user);

    }

    @Period
    @Count
    public List<UserDto> findUsers(UserCriteria criteria) {

        List<_User> result = getUsersByCriteria(criteria);
        return result.stream().limit(criteria.getSize())
                .skip((long) criteria.getPage() * criteria.getSize())
                .map(_User::convert)
                .collect(Collectors.toList());
    }

    @Period
    @Count
    private List<_User> getUsersByCriteria(UserCriteria criteria) {
        List<String> criteriaList = getCriteriaList(criteria);

        List<_User> result = new LinkedList<>();

        criteriaList.forEach(s -> {
            userList.forEach(user -> {
                if (user.getFirstName().contains(s)) {
                    if (isPresent(result, user)) {
                        result.add(user);
                    }
                }
                if (user.getLastName().contains(s)) {
                    if (isPresent(result, user)) {
                        result.add(user);
                    }
                }
                if (user.getUsername().contains(s)) {
                    if (isPresent(result, user)) {
                        result.add(user);
                    }
                }
                if (String.valueOf(user.getBirthDate()).contains(s)) {
                    if (isPresent(result, user)) {
                        result.add(user);
                    }
                }
            });
        });
        return result;
    }

    @Period
    @Count
    private boolean isPresent(List<_User> list, _User criteria) {
        boolean isPresent = true;
        if (list.size() > 0) {
            for (_User user : list) {
                if (user.getId().equals(criteria.getId())) {
                    isPresent = false;
                    break;
                }
            }
        }
        return isPresent;
    }

    @Period
    @Count
    private List<String> getCriteriaList(UserCriteria userCriteria) {
        List<String> search = new ArrayList<>();
        if (hasText(userCriteria.getFirstName())) {
            search.add(userCriteria.getFirstName());
        }
        if (hasText(userCriteria.getLastName())) {
            search.add(userCriteria.getLastName());
        }
        if (hasText(userCriteria.getUsername())) {
            search.add(userCriteria.getUsername());
        }
        if (hasText(String.valueOf(userCriteria.getBirthDate()))) {
            search.add(String.valueOf(userCriteria.getBirthDate()));
        }
        return search;
    }
}
