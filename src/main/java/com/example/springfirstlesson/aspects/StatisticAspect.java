package com.example.springfirstlesson.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.*;

import static java.lang.System.currentTimeMillis;
import static java.util.stream.Collectors.toMap;

@Aspect
@Component
public class StatisticAspect {
    public Map<String, Integer> counterData = new HashMap<>();
    public final Map<String, Integer> mostUsedMethods = getMostUsedMethods(counterData);
    public Map<String, Long> periodData = new HashMap<>();

    public long periodAvgData = getMethodsPeriodAvg(periodData);

    @Before("@annotation(Count)")
    public void writerLogicForCounter(JoinPoint joinPoint) {
        if (counterData.containsKey(joinPoint.toLongString())) {
            counterData.put(joinPoint.toLongString(), counterData.get(joinPoint.toLongString()) + 1);
        } else {
            counterData.put(joinPoint.toLongString(), 1);
        }
    }


    @Around("@annotation(Period)")
    public Object calculatePeriodAvg(ProceedingJoinPoint joinPoint) {

        long timeBeforeExecution = currentTimeMillis();
        String methodName = joinPoint.toLongString().substring(10, joinPoint.toLongString().length() - 1);
        try {
            joinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }


        if (periodData.containsKey(methodName)) {
            periodData.put(methodName, periodData.get(methodName) + currentTimeMillis() - timeBeforeExecution);
        } else {
            periodData.put(methodName, currentTimeMillis() - timeBeforeExecution);
        }

        return "OK";
    }

    private Long getMethodsPeriodAvg(Map<String, Long> map){
        for (var entry : map.entrySet()) {
            periodAvgData += entry.getValue();
        }
        return periodAvgData;
    }

    private Map<String, Integer> getMostUsedMethods(Map<String, Integer> map) {
        return map
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, HashMap::new));
    }
}
